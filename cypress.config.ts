import { defineConfig } from 'cypress';
import astroConfig from './astro.config';

export default defineConfig({
  component: {
    devServer: {
      framework: 'react',
      bundler: 'vite',
      viteConfig: astroConfig.vite,
    },
    video: false,
    screenshotOnRunFailure: false,
  },
});
