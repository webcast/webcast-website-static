import { readdirSync } from 'fs';

/**
 * Imports the configuration file for a given event ID.
 * @param eventId - The ID of the event to import the configuration for.
 * @returns A promise that resolves to the configuration object.
 */
export async function importConfig(eventId: string): Promise<ConfigType> {
  return await import(`./../events/${eventId}/config.json`);
}

/**
 * Lists the available events by reading the contents of the events directory.
 * @returns An array of strings representing the available event IDs.
 */
export function listAvailableEvents(): string[] {
  const events = readdirSync('./src/events');
  return events.filter((event) => event !== 'index');
}
