import { useEffect, useRef } from 'react';
// @ts-ignore
import getCernCustomPluginsContext from '@cern-vc/cern-paella-plugins';
// @ts-ignore
import getBasicPluginContext from 'paella-basic-plugins';
// @ts-ignore
import { Paella } from 'paella-core';
import {
  customtGetVideoIdFunction,
  customGetManifestFileUrlFunction,
  customLoadVideoManifestFunction,
} from '@/utils/paella-helpers';
import React from 'react';

type PlayerProps = ConfigType;

export default function Player(playerProps: PlayerProps) {
  const videoRef = useRef(null);
  const playerRef = useRef(null);

  // https://webcast.web.cern.ch/event/i991843?app=livehd&camera&slides

  useEffect(() => {
    const loadPaella = async () => {
      console.debug('Initializing Paella Player plugins context...');
      if (!playerRef.current) {
        const videoElement = videoRef.current;
        if (!videoElement) {
          return;
        }

        const initParams = {
          // Initialization callbacks
          configUrl: '/config/config.json', // config.json file url
          // loadConfig: defaultLoadConfigFunction, // overrides the config.json file load
          getVideoId: customtGetVideoIdFunction, // get the video identifier
          // getManifestUrl: defaultGetManifestUrlFunction, // get the video manifest url
          getManifestFileUrl: customGetManifestFileUrlFunction, // get the full manifest file url
          loadVideoManifest: () => customLoadVideoManifestFunction(playerProps), // get the manifest file content
          customPluginContext: [
            getBasicPluginContext(),
            getCernCustomPluginsContext(),
          ],
        };

        console.log('Initializing Paella Player plugins context... OK');
        const player = new Paella(videoRef.current, initParams);
        console.log('Initializing Paella Player... OK');

        try {
          await player.loadManifest();
          console.log('Loading video manifest... OK');
          playerRef.current = player;
        } catch (error) {
          console.log(error);
        }
      }
    };
    loadPaella();
  }, [playerProps]);

  // Remove the player when the functional component unmounts
  React.useEffect(() => {
    const player = playerRef.current;
    return () => {
      if (player) {
        console.log('Unmount player');
        playerRef.current = null;
      }
    };
  }, [playerRef]);

  if (!videoRef) {
    return <div>Loading player...</div>;
  }

  return (
    <div
      ref={videoRef}
      className="player-container"
      style={{
        minHeight: '600px',
      }}
    />
  );
}
