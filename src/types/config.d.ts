type ConfigType =
  | {
      streams: 'camera';
      cameraUrl: string;
      slidesUrl?: string | null;
      title: string;
    }
  | {
      streams: 'slides';
      cameraUrl?: string | null;
      slidesUrl: string;
      title: string;
    }
  | {
      streams: 'camera_slides';
      cameraUrl: string;
      slidesUrl: string;
      title: string;
    };
