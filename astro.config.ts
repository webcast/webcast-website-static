import { defineConfig } from 'astro/config';
import react from '@astrojs/react';
import tsconfigPath from 'vite-tsconfig-paths';

// https://astro.build/config
export default defineConfig({
  // Enable React to support React JSX components.
  integrations: [react()],
  vite: {
    plugins: [tsconfigPath()],
  },
  outDir: './build',
});
