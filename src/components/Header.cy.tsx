import Header from '@/components/Header';

describe('Header component', () => {
  it('renders with title prop', () => {
    const title = 'Test Title';
    cy.mount(<Header title={title} />);
    cy.contains('Webcast').should('exist');
    cy.contains(title).should('exist');
  });
});
