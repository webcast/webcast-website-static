#!/bin/bash

src_dir="./src/events"
dist_dir="./build/event"

# Iterate through each subdirectory in src_dir
for dir in "$src_dir"/*/; do
  # Get the id from the directory name
  id=$(basename "$dir")

  # Check if .htaccess file exists in the source directory
  if [ -f "$dir/.htaccess" ]; then
    # Create the destination directory if it doesn't exist
    mkdir -p "$dist_dir/i$id"

    # Copy .htaccess to the destination directory
    cp "$dir/.htaccess" "$dist_dir/i$id/.htaccess"

    echo "Copied .htaccess from $dir to $dist_dir/i$id/"
  else
    echo "No .htaccess found in $dir"
  fi
done
