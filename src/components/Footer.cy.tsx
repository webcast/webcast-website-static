import Footer from '@/components/Footer';
import packageJson from '@/../package.json';

describe('<Footer />', () => {
  it('renders', () => {
    // see: https://on.cypress.io/mounting-react
    cy.mount(<Footer />);
    // Expect CERN to be in the document
    cy.contains('CERN');
    // Expect the version to be in the document
    cy.contains(packageJson.version);
  });
});
