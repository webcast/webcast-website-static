import { Segment } from 'semantic-ui-react';
import packageJson from '@/../package.json';

export default function Footer() {
  return (
    <Segment inverted>
      <p>
        <a
          href="http://cern.ch/copyright"
          target="_blank"
          title="CERN copyright policy"
          rel="noreferrer"
        >
          {2019} - {new Date().getFullYear()} © Copyright CERN
        </a>{' '}
        - Version {packageJson.version}
      </p>
    </Segment>
  );
}
