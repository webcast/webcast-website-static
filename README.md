# Static Webcast Website

<p align="center"><img src="live-bw.png" alt="Video logo" height="200"></p>

Static version of the Webcast Website that allows it's users to watch a webcast directly.

It is built using React, Astro and Paella Player 7.

## Usage

The events are automatically loaded from the `src/events` folder.

They are created automatically using the Gitlab API from the `webcast` repository.

---

The different events are configured in the `src/events/{{event_id}}/config.json` file.

The format of the file is the following:

```json
{
  "streams": "camera_slides",
  "cameraUrl": "https://wowza.cern.ch/vod/_definist_/Video/Public/WebLectures/2022/1135177c8/1135177c8_desktop_camera.smil/playlist.m3u8",
  "slidesUrl": "https://wowza.cern.ch/vod/_definist_/Video/Public/WebLectures/2022/1135177c8/1135177c8_desktop_slides.smil/playlist.m3u8",
  "title": "10 years of Higgs boson measurements at ATLAS and CMS"
}
```

- `streams`: should be either `camera`, `slides` or `camera_slides`
- `cameraUrl`: URL of the camera stream, could be unset or set to `""`, `null` if `streams` is not `camera
- `slidesUrl`: URL of the slides stream, could be unset or set to `""`, `null` if `streams` is not `slides
- `title`: Title of the event

You can add as many events as you want, just create a new folder in `src/events/{{event_id}}` and add the `config.json` file.

If you want to enable protection for the event, you can add a `.htaccess` file in the event folder with the following content:

```
AuthType openid-connect
Require claim cern_roles:default-role
```

This will protect the event with the default role of the user. You can change the role by changing the `default-role` value.

Then you can run the website using:

```bash
npm run build
npm run preview
```

And open the website in your browser http://localhost:3000/

## How to manually add an event - Step by step

Imagine that you want to add an event with this Event ID: `123456789`

1. Create a new folder in `src/events/123456789`
2. Create a `config.json` file in `src/events/123456789` with the following content:

```json
{
  "streams": "camera_slides",
  "cameraUrl": "<camera_url>",
  "slidesUrl": "<slides_url>",
  "title": "<event_title>"
}
```

3. Add the camera and slides URLs to the `config.json` file
4. Add the event title to the `config.json` file
5. (Optional) Add a `.htaccess` file in the `src/events/123456789` folder with the following content:

```
AuthType openid-connect
Require claim cern_roles:<role-identifier>
```

6. Add the role identifier to the `.htaccess` file, by default it is `default-role`
7. Build the website using `npm run build`
8. Run the website using `npm run preview`
9. Open the website in your browser: http://localhost:3000/event/i123456789 (Note the `i` before the event ID)

---

If you want to manually add the event to the website, you can commit and push the changes and the website will be automatically updated.

If Gitlab CI is not working, you can manually deploy the new created event by coping the content of the `build\event\i123456789` folder to where the website is hosted.

## Development

### Requirements

- Node 16.15.x
- NPM

### Install dependencies

```bash
npm install
```

### Run development server

```bash
npm run dev
```

### Build for production

```bash
npm run build
```

## Testing

To run the tests, you can use the following command:

```bash
npm run test
```

Tests are made using cypress and vitest.

---

## Attributions

- Logo: <a href="https://www.flaticon.es/iconos-gratis/television" title="televisión iconos">Icon created by Freepik - Flaticon</a>
