import {
  createLiveCameraManifest,
  createLiveCameraSlidesManifest,
  createLiveSlidesManifest,
} from '@/utils/paella-manifests';

export async function customGetManifestFileUrlFunction(): Promise<string> {
  console.log('Using custom getManifestFileUrl function');
  return '';
}

export async function customLoadVideoManifestFunction({
  streams,
  cameraUrl,
  slidesUrl,
  title,
}: ConfigType): Promise<any> {
  console.debug('Using custom loadVideoManifest function');

  function getManifest(): string {
    switch (streams) {
      case 'camera':
        return createLiveCameraManifest(title, cameraUrl);
      case 'slides':
        return createLiveSlidesManifest(title, slidesUrl);
      case 'camera_slides':
        return createLiveCameraSlidesManifest(title, cameraUrl, slidesUrl);
    }
  }

  const manifest = getManifest();
  return JSON.parse(manifest);
}

export async function customtGetVideoIdFunction(): Promise<string> {
  console.log('Using custom getVideoId function');
  return '';
}

export async function defaultLoadConfigFunction(
  configUrl: string,
  player: any,
): Promise<any> {
  console.log('Using default configuration loading function.');
  console.log(player);
  const response = await fetch(configUrl);
  return response.json();
}

// repoUrl: the value specified in initParams.repositoryUrl
// videoId: the video identifier returned by initParams.getVideoId()
export async function defaultGetManifestUrlFunction(
  repoUrl: string,
  videoId: string,
  config: any,
  player: any,
): Promise<string> {
  console.log('Using default getManifestUrl function');
  console.log(repoUrl, videoId, config, player);
  return '';
}

export async function defaultLoadVideoManifestFunction(
  videoManifestUrl: string,
  config: any,
  player: any,
): Promise<any> {
  console.log('Using default loadVideoManifest function');
  console.log(config, player);
  const response = await fetch(videoManifestUrl);
  return response.json();
}
