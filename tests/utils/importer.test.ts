import { describe, it, expect } from 'vitest';
import { importConfig, listAvailableEvents } from '@/utils/importer';

describe('importConfig', () => {
  it('should import the configuration file for a given event ID', async () => {
    const config = await importConfig('index');
    expect(config).toBeDefined();
    expect(config.title).toBe(
      '10 years of Higgs boson measurements at ATLAS and CMS',
    );
  });

  it('should throw an error if the configuration file does not exist', async () => {
    await expect(importConfig('nonexistent')).rejects.toThrow();
  });
});

describe('listAvailableEvents', () => {
  it('should list the available events', () => {
    const events = listAvailableEvents();
    expect(events).not.toContain('index');
  });
});
