import Player from '@/components/Player';
import importedConfig from '@/events/index/config.json';

describe('Player', () => {
  it('renders correctly', () => {
    const config = importedConfig as ConfigType;
    cy.mount(<Player {...config} />);
    cy.get('div.player-container').should('exist');
  });
});
