import { Header as SemHeader, Image, Segment } from 'semantic-ui-react';

interface HeaderProps {
  title: string;
}

export default function Header({ title }: HeaderProps) {
  return (
    <Segment inverted>
      <SemHeader as="h2" inverted>
        <Image src="/images/cern_80_white.png" />
        <SemHeader.Content>
          Webcast
          <SemHeader.Subheader>{title}</SemHeader.Subheader>
        </SemHeader.Content>
      </SemHeader>
    </Segment>
  );
}
